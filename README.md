## 项目说明
短链生成服务




## 技术选型

- 核心框架：Spring Boot 1.5.x

- 视图框架：Spring MVC 4.3

- 持久层框架：JPA

- 数据库：MySQL、redis

- lombok




## 本地部署

- 通过git下载源码
- 创建数据库shorturl，数据库编码为UTF-8
- 执行db/db.sql文件，初始化数据
- 修改application-{env}.yml，更新MySQL账号和密码
- Eclipse、IDEA运行ShorturlApplication.java，则可启动项目