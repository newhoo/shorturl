package io.github.flylinran.common.exception;

public class RequestLimitException extends BaseException {
    public RequestLimitException(int code, String msg) {
        super(code, msg);
    }
}
