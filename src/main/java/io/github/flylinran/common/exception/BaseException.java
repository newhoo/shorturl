package io.github.flylinran.common.exception;

import lombok.Getter;
import lombok.Setter;

/**
 * 自定义异常
 */
@Getter @Setter
public class BaseException extends RuntimeException {

    private String msg;
    private int code = 500;

    public BaseException(String msg) {
        super(msg);
        this.msg = msg;
    }

    public BaseException(String msg, Throwable e) {
        super(msg, e);
        this.msg = msg;
    }

    public BaseException(int code, String msg) {
        super(msg);
        this.msg = msg;
        this.code = code;
    }

    public BaseException(int code, String msg, Throwable e) {
        super(msg, e);
        this.msg = msg;
        this.code = code;
    }
}
