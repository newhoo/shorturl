package io.github.flylinran.modules.app.dao;

import io.github.flylinran.modules.app.entity.UrlEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UrlDao extends JpaRepository<UrlEntity, Integer> {
}
