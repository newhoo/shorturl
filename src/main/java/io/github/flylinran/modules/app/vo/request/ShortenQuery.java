package io.github.flylinran.modules.app.vo.request;

import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

@Data
public class ShortenQuery {
    @NotEmpty(message = "访问码不能为空")
    private String accessCode;
    @NotEmpty(message = "URL不能为空")
    private String url;
}
