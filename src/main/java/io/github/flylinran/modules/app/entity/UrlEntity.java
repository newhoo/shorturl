package io.github.flylinran.modules.app.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;

@Entity
@Table(name = "t_url")
@Data @Accessors(chain = true)
public class UrlEntity {
    @Id
    @GeneratedValue
    private Integer id;

    private String code;

    @Column(length = 1024)
    private String url;
}
